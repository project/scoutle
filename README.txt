
-- SUMMARY --

Support for the Scoutle social networking service.  It enables the use
of Scoutle stages that can be embedded in websites which connect blogs
to other related blogs.  For a website to embed a stage, the website
has to be registered with a scout on Scoutle, which will give you the
scout hash necessary for configuring the stage.

The website for Scoutle is at http://www.scoutle.com/

This module is limited to a single scout per Drupal installation, so
it will not be appropriate for Drupal sites with multiple blogs which
have separate scouts, i.e. if the blogs are registered separately at
Scoutle.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Register your Drupal site with Scoutle if you have not already done so.

* Configure user permissions in Administer >> User management >>
  Permissions >> scoutle module:

  - access scoutle stage

    Users in roles with the "access scoutle stage" permission will be
    able to see the Scoutle stage if available.  As Scoutle is useful
    as a means of connecting bloggers to other unknown blogs that may
    be potentially interesting, this permission should normally be
    granted to all users.

  - administer scoutle

    Users in roles with the "administer scoutle" permission will be
    able to configure the settings for the Scoutle stage.

* Configure the Scoutle stage in Administer >> Site configuration >>
  Scoutle by specifying the scout hash.  Optionally, set the type of
  stage you want for your website.

  If you do not already know the scout hash, it should be found at the
  Scoutle website in My Blogs >> Edit (for the corresponding blog) >>
  Customize my Scout and Stage/Widget >> Stage and Widget Installation
  >> Next >> Drupal.

* Configure where you want the Scoutle stage to appear in Administer
  >> Site building >> Blocks by configuring the Scoutle block.


-- CUSTOMIZATION --

* Scoutle offers the option of configuring the background color of the
  stage on their own site.  This module does not offer customization
  options beyond setting the type of stage embedded in the website.


-- CONTACT --

Current maintainer:
* Yoo Chung (chungyc) - http://drupal.org/user/410478
